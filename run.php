<?php

require_once __DIR__ . '/vendor/autoload.php';

use App\Application\Domain\Image;
use App\Infrastructure\Database\FileImages;
use App\Infrastructure\Finder\ImageFinder;
use App\Infrastructure\Logger\FileLogger;

function find(FileImages $db, ImageFinder $finder): string
{
    $src = $finder->find();
    if ($db->has($src)) {
        return find($db, $finder);
    }

    return $src;
}

$logger = new FileLogger();

try {
    $db = new FileImages();
    $finder = new ImageFinder();

    $saved = 0;

    while($saved < 3) {
        $db->add(new Image(find($db, $finder)));
        $saved++;
    }

    $db->save();
} catch (Exception $e) {
    $logger->log($e->getMessage());
}

