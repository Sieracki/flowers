<?php

namespace App\Application\Domain;

use App\Application\Database\Id;

final class Image
{
    private Id $id;
    private string $path;

    public function __construct(string $path)
    {
        $this->id = new Id();
        $this->path = $path;
    }

    public function getPath(): string
    {
        return $this->path;
    }
}
