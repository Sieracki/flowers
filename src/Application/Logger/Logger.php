<?php

namespace App\Application\Logger;

interface Logger
{
    public function log(string $msg): void;
}
