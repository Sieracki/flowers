<?php

namespace App\Application\Database;

use Ramsey\Uuid\Uuid;

final class Id
{
    private string $value;

    public function __construct()
    {
        $this->value = Uuid::uuid4()->toString();
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
