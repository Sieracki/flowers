<?php

namespace App\Application\Database\Repository;

use App\Application\Domain\Image;

interface Images
{
    public function connect(): void;

    public function add(Image $image): void;

    public function has(string $path): bool;

    public function save(): void;

    public function getAll(): array;
}
