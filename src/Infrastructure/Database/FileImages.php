<?php

namespace App\Infrastructure\Database;

use App\Application\Database\Repository\Images;
use App\Application\Domain\Image;

final class FileImages implements Images
{
    private const DB_ROOT = __DIR__ . '/../../../db/db.txt';

    private array $data;
    private string $dbRoot;

    public function __construct(string $dbRoot = self::DB_ROOT)
    {
        $this->data = [];
        $this->dbRoot = $dbRoot;
        $this->connect();
    }

    public function connect(): void
    {
        if (file_exists($this->dbRoot)) {
            $this->data = unserialize(file_get_contents($this->dbRoot));
        }
    }

    public function add(Image $img): void
    {
        if ($this->has($img->getPath())) {
            return;
        }

        $this->data[] = $img;
    }

    public function save(): void
    {
        file_put_contents($this->dbRoot, serialize($this->data));
    }

    public function getAll(): array
    {
        return $this->data;
    }

    public function has(string $path): bool
    {
        return count(array_filter($this->data, fn (Image $img) => $img->getPath() === $path)) > 0;
    }
}
