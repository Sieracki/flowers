<?php

namespace App\Infrastructure\Logger;

use App\Application\Logger\Logger;

final class FileLogger implements Logger
{
    private const LOG_PATH = __DIR__ . '/../../../log/log.err';
    private string $logPath;

    public function __construct(string $logPath = self::LOG_PATH)
    {
        $this->logPath = $logPath;
    }

    public function log(string $msg): void
    {
        file_put_contents($this->logPath, $msg . PHP_EOL, FILE_APPEND);
    }
}
