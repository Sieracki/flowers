<?php

namespace App\Infrastructure\Finder;

use DOMDocument;
use DOMXPath;

final class ImageFinder
{
    private DOMXPath $xpath;

    public function __construct()
    {
        $domDoc = new DOMDocument();
        @$domDoc->loadHTMLFile('https://sklep.swiatkwiatow.pl/');

        $this->xpath = new DOMXPath($domDoc);
    }

    public function find(): string
    {
        $images = $this->xpath->query('//img');

        return $images->item(rand(0, $images->length - 1))->getAttribute('src');
    }
}
