<?php

namespace Test\Infrastructure;

use App\Application\Domain\Image;
use App\Infrastructure\Database\FileImages;
use PHPUnit\Framework\TestCase;

class FileImagesTest extends TestCase
{
    public function testFirstConnectionWithDb(): void
    {
        $db = new FileImages(__DIR__ . '/var/first.txt');
        $db->connect();

        $this->assertCount(0, $db->getAll());
    }

    public function testAddImage(): void
    {
        $path = '/var/existing.txt';
        $db = new FileImages(__DIR__ . $path);
        $db->add(new Image('path'));

        $this->assertCount(1, $db->getAll());
        $db->save();
        $this->assertFileExists(__DIR__ . $path);
    }

    public function testDbPersistence(): void
    {
        $path = '/var/persist.txt';
        $db = new FileImages(__DIR__ . $path);
        $db->add(new Image('path'));
        $db->save();

        $assertDb = new FileImages(__DIR__ . $path);
        $this->assertCount(1, $assertDb->getAll());
    }
}
