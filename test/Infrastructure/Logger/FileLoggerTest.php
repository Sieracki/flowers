<?php

namespace Test\Infrastructure\Logger;

use App\Infrastructure\Logger\FileLogger;
use PHPUnit\Framework\TestCase;

final class FileLoggerTest extends TestCase
{
    public function testLogError(): void
    {
        $logPath = __DIR__ . '/var/log.err';
        if (file_exists($logPath)) {
            unlink($logPath);
        }

        $logger = new FileLogger($logPath);
        $logger->log('Test msg');
        $logger->log('Second test msg');
        $this->assertFileExists($logPath);

        $expected = <<<TXT
Test msg
Second test msg

TXT;

        $this->assertEquals($expected, file_get_contents($logPath));
    }
}
